from django.shortcuts import render
from django.conf import settings

import requests
from bs4 import BeautifulSoup

# Create your views here.

jsversion = settings.JSVERSION
cssversion = settings.CSSVERSION

def homepage(request):
    list_category = settings.LIST_CATEGORY
    obj_template = {
        'emptyContent': False,
        'list_category': list_category,
        'tab_active': 0,
        'jsversion': jsversion,
        'cssversion': cssversion
    }
    homepage_content = parseMainContent(True, '')
    list_content_new_best_shirt = parseNewBestShirtContent(homepage_content)
    if len(list_content_new_best_shirt) > 0:
        try:
            new_shirt_text = list_content_new_best_shirt[1].find('strong').text
            new_shirt_content = list_content_new_best_shirt[2].find_all(
                'a') 
            best_selling_text = list_content_new_best_shirt[
                3].find('strong').text
            best_selling_content = list_content_new_best_shirt[4].find_all('a')
        except Exception as err:
            obj_template['emptyContent'] = True
            obj_template['list_img_src'] = []
            print err
        else:
            obj_template['new_shirt_text'] = new_shirt_text
            obj_template['list_img_src_new'] = setArrImgContent(
                new_shirt_content)
            obj_template['best_selling_text'] = best_selling_text
            obj_template['list_img_src_best'] = setArrImgContent(
                best_selling_content)
    else:
        obj_template['emptyContent'] = True
    return render(request, 'index.html', obj_template)


def pagebasedcategory(request, id_category=1, display_url=''):
    list_category = settings.LIST_CATEGORY
    obj_template = {
        'emptyContent': False,
        'list_category': list_category,
        'tab_active': 1,
        'jsversion': jsversion,
        'cssversion': cssversion
    }
    url_category = getUrlCategory(id_category)
    page_category_content = parseMainContent(False, url_category)
    list_content_page_category = parseContentPageCategory(
        page_category_content)
    if len(list_content_page_category) > 0:
        start = 1
        if id_category == '18':
            start = 0
        dataPerCategory = generateObjContCategory(list_content_page_category, start)
        if len(dataPerCategory) > 0:
            obj_template['dataPerCategory'] = dataPerCategory
        else:
            obj_template['emptyContent'] = True
            obj_template['dataPerCategory'] = []
    else:
        obj_template['emptyContent'] = True
    return render(request, 'page_base_on_category.html', obj_template)


def about_shirt(request):
    list_category = settings.LIST_CATEGORY
    obj_template = {
        'list_category': list_category,
        'tab_active': 2,
        'jsversion': jsversion,
        'cssversion': cssversion
    }
    return render(request, 'about_shirt.html', obj_template)

# public function


def validateRequest(url):
    req = requests.get(url)
    if req.status_code == 200:
        return req
    elif req.status_code == 404:
        return '404'


def parseMainContent(homepage, url):
    url_req = ''
    if homepage:
        url_req = validateRequest(settings.SHIRT_URL)
    else:
        url_req = validateRequest(settings.SHIRT_URL + '/' + url + '/')
    if url_req != '404':
        url_content = url_req.content
        html = BeautifulSoup(url_content, 'html.parser')
        return html
    else:
        return ''


def parseNewBestShirtContent(content):
    try:
        new_best_produk_content = content.find('article', id='post-589').find(
            'div', attrs={'class': 'entry-content cf'})
        list_new_best_produk = new_best_produk_content.find_all('p')
    except Exception as err:
        list_new_best_produk = []

    return list_new_best_produk


def parseContentPageCategory(content):
    try:
        list_content_page_category = content.find(
            'div', attrs={'class': 'entry-content cf'}).find_all('p')
    except Exception as err:
        list_content_page_category = []

    return list_content_page_category


def generateObjContCategory(contents, start_counter):
    arr_obj_category = []
    obj_category = {}
    obj_category['title_category'] = ''
    for counter in range(start_counter, len(contents)):
        list_content_img = contents[counter].find_all('a')
        if len(list_content_img) > 0:
            arr_img = []
            for a in list_content_img:
                obj_img = {}
                content_img = a.find('img')
                if content_img == None:
                    pass
                else:
                    obj_img['url_img_big'] = a['href']
                    obj_img['url_img'] = a.find('img')['src']
                    arr_img.append(obj_img)
            obj_category['list_img_src'] = arr_img
        else:
            try:
#               try find strong element
                obj_category['title_category'] = contents[
                    counter].find('strong').text.encode('utf-8')
            except:
                obj_category['title_category'] = contents[
                    counter].text.encode('utf-8')
        if obj_category.has_key('list_img_src'):
            print 'title_category : ',obj_category['title_category'], ' jumlah : ', len(obj_category['list_img_src'])
            arr_obj_category.append(obj_category)
            obj_category = {}
            obj_category['title_category'] = ''

    return arr_obj_category


def setArrImgContent(contents):
    arr_img = []
    for content in contents:
        obj_img = {}
        obj_img['url_img_big'] = content['href']
        obj_img['url_img'] = ''
        if content.img != None:
            obj_img['url_img'] = content.find('img')['src']
        arr_img.append(obj_img)

    return arr_img


def getUrlCategory(id_category):
    obj_category = settings.LIST_CATEGORY[int(id_category) - 1]
    url_category = obj_category['url']

    return url_category
