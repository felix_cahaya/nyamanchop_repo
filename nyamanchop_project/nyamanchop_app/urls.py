from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.homepage, name='homepage'),
    url(r'^tentang-kaos/$', views.about_shirt, name='about_shirt'),
    url(r'^(?P<id_category>[0-9]+)/(?P<display_url>.*)/$', views.pagebasedcategory, name='categorypage')
]