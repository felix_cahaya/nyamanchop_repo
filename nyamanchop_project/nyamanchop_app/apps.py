from django.apps import AppConfig


class NyamanchopAppConfig(AppConfig):
    name = 'nyamanchop_app'
