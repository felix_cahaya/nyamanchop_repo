$('#imgModal').on('show.bs.modal', function(e) {
	$('#big-img	').attr('src', e.relatedTarget.getAttribute('data-url'));
	$('#myModalLabel').html(e.relatedTarget.getAttribute('data-title'));
});

function backToTop () {
	document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

function scrolling() {
	if (document.body.scrollTop > 150 || document.documentElement.scrollTop > 150) {
        document.getElementById("btn-back-top").style.display = "block";
    } else {
        document.getElementById("btn-back-top").style.display = "none";
    }
}

window.onscroll = function () {
	scrolling()
};